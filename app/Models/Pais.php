<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Grupo;
use App\Models\Partido;

class Pais extends Model
{
    use HasFactory;
    protected $table = 'paises';

    public function grupo(){
        return $this->belongsTo(Grupo::class);
    }

    public function partidos(){
        return $this->belongsToMany(Partido::class);
    }

    // public function partidos(){
    //     return $this->belongsTo(Partido::class, 'pais_id1')->get()->merge($this->belongsTo(Patido::class, 'pais_id2')->get());
    // }
}
