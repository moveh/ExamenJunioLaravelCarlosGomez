<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\Relations\BelongsToMany;
use App\Models\Pais;

class Partido extends Model
{
    use HasFactory;
    protected $table = 'partidos';

    public function paises(){
        return $this->belongsToMany(Pais::class);
    }

    public function grupos(){
        return $this->hasMany(Grupo::class);
    }

}
