<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use App\Models\Pais;

class Grupo extends Model
{
    use HasFactory;
    protected $table = 'grupos';

    public function paises(){
        return $this->hasMany(Pais::class);
    }

    // public function partidos(){
    //     return 
    // }
}
