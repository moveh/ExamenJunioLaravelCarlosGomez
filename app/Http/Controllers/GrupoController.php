<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Grupo;

class GrupoController extends Controller
{
    function index()
    {
        $grupos = Grupo::all();
        return view('grupos.index')->with('grupos', $grupos);
    }
    public function store(Request $a)
    {

        $grupo = new Grupo();
        $grupo->letra = $a->letra;
        $grupo->save();

        return redirect()->route('grupos.index', $grupo);
    }

    public function update(Request $a, int $id)
    {
        $grupo = Grupo::find($id);
        $grupo->letra = $a->letra;
        $grupo->save();

        return redirect()->route('grupos.index');
    }
}
