<?php

namespace Database\Factories;

use App\Models\Jugador;
use App\Models\Pais;
use Illuminate\Database\Eloquent\Factories\Factory;
use Illuminate\Support\Str;

class JugadorFactory extends Factory
{
    /**
     * The name of the factory's corresponding model.
     *
     * @var string
     */
    protected $model = Jugador::class;

    /**
     * Define the model's default state.
     *
     * @return array
     */
    public function definition()
    {
        $nombre = $this->faker->name('male');
        $camiseta = $this->faker->numberBetween(0, 25);
        $fnac = $this->faker->dateTimeBetween('-35 years', '+18 years');
        $posicion = $this->faker->randomElement(array('POR', 'DEF', 'CEN', 'DEL'));

        return [
            'nombre' => $nombre,
            'numeroCamiseta' => $camiseta,
            'fechaNacimiento' => $fnac,
            'posicion' => $posicion,
            'pais_id' => Pais::all()->random()->id
        ];
    }
}
