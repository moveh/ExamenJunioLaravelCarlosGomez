@extends('layouts.master')
@section('titulo')
    Eurocopa
@endsection
@section('contenido')
<div class="row">
    @foreach( $grupos as  $grupo )
        <div class="col-xs-12 col-sm-6 col-md-4 mb-4">
            <div class="card text-white bg-dark" style="width: auto;">
                <div class="card-body">
                    <h4 class="card-title text-center">{{$grupo->letra}}</h4>
                </div>
                <ul class="list-group list-group-flush text-dark">
                    @foreach ($grupo->paises as $pais)
                        <li class="list-group-item">{{$pais->nombre}}</li> 
                    @endforeach
                </ul>
            </div>
        </div>
    @endforeach
</div>
@endsection