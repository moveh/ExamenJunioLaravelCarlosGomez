<?php

use Illuminate\Support\Facades\Route;
use App\Http\Controllers\InicioController;
use App\Http\Controllers\JugadorController;
use App\Http\Controllers\PaisController;
use App\Http\Controllers\GrupoController;
use App\Http\Controllers\PartidoController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', [InicioController::class, 'inicio'])->name('home');

Route::get('grupos', [GrupoController::class, 'index'])->name('grupos.index');

Route::get('paises/{pais}', [PaisController::class, 'show']) -> name('paises.show');

Route::get('partido/{partido}/disputar', [PartidoController::class, 'show']) -> name('partido.show');

// Route::middleware(['auth:sanctum', 'verified'])->get('futbolistas/create', [FutbolistaController::class, 'create'])->name('futbolistas.create');

// Route::get('futbolistas/{futbolista}', [FutbolistaController::class, 'show'])->name('futbolistas.show');

// Route::middleware(['auth:sanctum', 'verified'])->get('futbolistas/{futbolista}/editar', [FutbolistaController::class, 'edit'])->name('futbolistas.edit');
